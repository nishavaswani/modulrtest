package codingtest.com.modulr.exceptions;

import org.slf4j.helpers.MessageFormatter;

import codingtest.com.modulr.utility.Note;

/**
 * @author Nisha.Vaswani
 *
 */
public class ATMOutOfNotesException extends Exception {
 
	private static final long serialVersionUID = -8904349435565850109L;

	private final String message;
 
	public ATMOutOfNotesException(final Note note) {
		final String message = 	MessageFormatter.arrayFormat("Not enough notes of value '{}' in ATM",new Object[] { note.getDenomination() }).getMessage();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
