package codingtest.com.modulr.exceptions;

import org.slf4j.helpers.MessageFormatter;


/**
 * @author Nisha.Vaswani
 *
 */
public class WithdrawalAmountTranslationToNotesException extends Exception {

	private static final long serialVersionUID = -8904349435565850109L;

	private final String message; 

	public WithdrawalAmountTranslationToNotesException(final long amount) {
		final String message = MessageFormatter.arrayFormat("Withdrawal amount '{}' cannot be translated to notes", new Object[] { amount } ).getMessage();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
