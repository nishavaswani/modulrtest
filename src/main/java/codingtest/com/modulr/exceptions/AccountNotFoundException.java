package codingtest.com.modulr.exceptions;

import org.slf4j.helpers.MessageFormatter;

/**
 * @author Nisha.Vaswani
 *
 */
public class AccountNotFoundException extends Exception {
 
	private static final long serialVersionUID = -8904349435565850109L;

	private final String message;

	public AccountNotFoundException(final String accountNumber) {
		final String message = 	MessageFormatter.arrayFormat("Wrong account number '{}'",new Object[] { accountNumber }).getMessage();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

}
