package codingtest.com.modulr.utility;

import java.util.Currency;

/**
 * @author Nisha.Vaswani
 *
 */
public enum Note {

	FIVE(5), 
	TEN(10), 
	TWENTY(20),  
	FIFTY(50);
	
	public static final Currency CURRENCY = Currency.getInstance("GBP");

	
	private int denomination;

	private Note(final int denomination) {
		this.denomination = denomination;
	}

	public int getDenomination() {
		return denomination;
	}
}


