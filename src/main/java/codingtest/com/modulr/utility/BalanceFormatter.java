package codingtest.com.modulr.utility;

import java.math.BigDecimal;

/**
 * @author Nisha.Vaswani
 *
 */
public interface BalanceFormatter {

	String formatBalance(BigDecimal account);
}
