package codingtest.com.modulr.utility;

import java.math.BigDecimal;

import org.slf4j.helpers.MessageFormatter;

/**
 * @author Nisha.Vaswani
 *
 */
public class BalanceFormatterImpl implements BalanceFormatter {

	@Override
	public String formatBalance(final BigDecimal accountBal) {
		final String balanceToDisplay =	MessageFormatter.arrayFormat("{} {}", new Object[] { 
				 				Note.CURRENCY.getSymbol(), //
								accountBal 
						} 
				).getMessage();
		return balanceToDisplay;
	}
}
