package codingtest.com.modulr.service.impl.note;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codingtest.com.modulr.exceptions.ATMOutOfNotesException;
import codingtest.com.modulr.service.note.NoteRepository;
import codingtest.com.modulr.utility.Note;
import codingtest.com.modulr.utility.NoteHelper;


/**
 * @author Nisha.Vaswani
 *
 */
public class NoteRepositoryImpl implements NoteRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(NoteRepositoryImpl.class);

	
	private Map<Note, Long> noteMap = new HashMap<>();

	
	public NoteRepositoryImpl() {
	}

	
	public NoteRepositoryImpl( final Collection<Note> noteList ) {
		addNoteList(noteList);
	}

	
	/*
	 * (non-Javadoc)
	 * @see codingtest.com.modulr.service.note.NoteRepository#addNoteList(java.util.Collection)
	 */
	@Override
	public void addNoteList(final Collection<Note> noteList) {
		// Assuming no limit for notes capacity
		for (Note note : noteList) {
			Long amount = noteMap.get(note);
			if (amount == null) {
				amount = Long.valueOf(0);
			}
			// increment
			// eliminating auto-boxing as much as possible
			amount = Long.valueOf(amount.longValue() + 1);//
			// store
			noteMap.put(note, amount);
			LOGGER.info("Added a {} note to persistent store", note);
		}
	}

	
/*
 * (non-Javadoc)
 * @see codingtest.com.modulr.service.note.NoteRepository#tryRemoveNoteList(java.util.Collection)
 */
	@Override
	public void tryRemoveNoteList(final Collection<Note> noteList) throws ATMOutOfNotesException {
		final Map<Note, Long> noteMap = NoteHelper.toNoteMap(noteList);
		for ( Map.Entry<Note, Long> noteMapEntry : noteMap.entrySet() ) {
			final Long amount = this.noteMap.get(noteMapEntry.getKey());
			if ( amount == null ) {
				final ATMOutOfNotesException error = new ATMOutOfNotesException(noteMapEntry.getKey());
				LOGGER.error( error.getMessage() );
				throw error;
			}
			if ( amount.longValue() < noteMapEntry.getValue().longValue() ) {
				final ATMOutOfNotesException error = new ATMOutOfNotesException( noteMapEntry.getKey() 	);
				LOGGER.error( error.getMessage() );
				throw error;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see codingtest.com.modulr.service.note.NoteRepository#removeNoteList(java.util.Collection)
	 */
	
	@Override
	public void removeNoteList(final Collection<Note> noteList) throws ATMOutOfNotesException {
		tryRemoveNoteList(noteList);
		for (Note note : noteList) {
			Long amount = noteMap.get(note);
			// decrement
			// eliminating auto-boxing as much as possible
			amount = Long.valueOf(amount.longValue() - 1);
			if (amount.longValue() == 0) {
				noteMap.remove(note);
			} else {
				noteMap.put(note, amount);
			}
			LOGGER.info("Removed a {} note from persistent store", note);
		}
	}

	
	@Override
	public Map<Note, Long> getDeposit() {
		return Collections.unmodifiableMap(noteMap);
	}
}
