package codingtest.com.modulr.service.impl.atm;

import java.math.BigDecimal;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codingtest.com.modulr.exceptions.ATMOutOfNotesException;
import codingtest.com.modulr.exceptions.AccountBalanceNotEnoughException;
import codingtest.com.modulr.exceptions.AccountNotFoundException;
import codingtest.com.modulr.exceptions.WithdrawalAmountNotAllowedException;
import codingtest.com.modulr.exceptions.WithdrawalAmountTranslationToNotesException;
import codingtest.com.modulr.service.account.AccountService;
import codingtest.com.modulr.service.atm.AtmService;
import codingtest.com.modulr.service.note.NoteRepository;
import codingtest.com.modulr.service.note.NoteService;
import codingtest.com.modulr.utility.BalanceFormatter;
import codingtest.com.modulr.utility.Note;
import codingtest.com.modulr.utility.NoteHelper;


/**
 * @author Nisha.Vaswani
 *
 */
public class AtmServiceImpl implements AtmService { 

	private static final Logger LOGGER = LoggerFactory.getLogger(AtmServiceImpl.class);


	private AccountService accountService;


	private NoteService noteService;


	private NoteRepository noteRepository;


	private BalanceFormatter balanceFormatter;


	private long minimumWithdrawalAmount = 20;

	private long maximumWithdrawalAmount = 250;

	public AtmServiceImpl(final AccountService accountService,final NoteService noteService, final NoteRepository noteRepository, final BalanceFormatter balanceFormatter) {
		this.accountService = accountService;
		this.noteService = noteService;
		this.noteRepository = noteRepository;
		this.balanceFormatter = balanceFormatter;
	}

	/*
	 * (non-Javadoc)
	 * @see codingtest.com.modulr.service.atm.AtmService#replenish(java.util.Collection)
	 */
	public void replenish(Collection<Note> noteList) {
		LOGGER.info("Replenishing ATM with notes: {}",NoteHelper.toNoteMap(noteList));
		noteRepository.addNoteList(noteList);
	}

	/*
	 * (non-Javadoc)
	 * @see codingtest.com.modulr.service.atm.AtmService#checkBalance(java.lang.String)
	 */
	public String checkBalance( final String accountNumber ) throws AccountNotFoundException {
		LOGGER.info("Checking balance of account '{}'", accountNumber );
		final BigDecimal accountBal = accountService.checkBalance(accountNumber);
		return balanceFormatter.formatBalance(accountBal);
	}

	/*
	 * (non-Javadoc)
	 * @see codingtest.com.modulr.service.atm.AtmService#withdrawAmount(java.lang.String, long)
	 */
	public Collection<Note> withdrawAmount(final String accountNumber,final long withdrawalAmount ) throws AccountNotFoundException, AccountBalanceNotEnoughException, 	ATMOutOfNotesException, WithdrawalAmountTranslationToNotesException, WithdrawalAmountNotAllowedException {
		LOGGER.info("Withdrawing amountParameter of '{}' from account '{}'...", withdrawalAmount, accountNumber );
		if (withdrawalAmount < minimumWithdrawalAmount	|| 	withdrawalAmount > maximumWithdrawalAmount ) {
			final WithdrawalAmountNotAllowedException error = new WithdrawalAmountNotAllowedException( withdrawalAmount, minimumWithdrawalAmount, maximumWithdrawalAmount );
			LOGGER.error( error.getMessage() );
			throw error;
		}

		final Collection<Note> noteList = noteService.translateWithdrawalAmount( withdrawalAmount, noteRepository );
		noteRepository.tryRemoveNoteList( noteList);
		accountService.withdrawAmount( accountNumber, withdrawalAmount );
		noteRepository.removeNoteList( noteList );
		return noteList;
	}
}

