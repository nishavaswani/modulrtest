package codingtest.com.modulr.service.atm;

import java.util.Collection;

import codingtest.com.modulr.exceptions.ATMOutOfNotesException;
import codingtest.com.modulr.exceptions.AccountBalanceNotEnoughException;
import codingtest.com.modulr.exceptions.AccountNotFoundException;
import codingtest.com.modulr.exceptions.WithdrawalAmountNotAllowedException;
import codingtest.com.modulr.exceptions.WithdrawalAmountTranslationToNotesException;
import codingtest.com.modulr.utility.Note;

/**
 * @author Nisha.Vaswani
 *
 */
public interface AtmService {

	void replenish(Collection<Note> noteList);

	String checkBalance(String accountNumber ) throws AccountNotFoundException;

	Collection<Note> withdrawAmount(String accountNumber,long withdrawalAmount) throws AccountNotFoundException, AccountBalanceNotEnoughException, ATMOutOfNotesException, WithdrawalAmountTranslationToNotesException, WithdrawalAmountNotAllowedException;
}



