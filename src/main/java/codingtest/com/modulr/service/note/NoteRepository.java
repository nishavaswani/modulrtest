package codingtest.com.modulr.service.note;

import java.util.Collection;
import java.util.Map;

import codingtest.com.modulr.exceptions.ATMOutOfNotesException;
import codingtest.com.modulr.utility.Note;


/**
 * @author Nisha.Vaswani
 *
 */
public interface NoteRepository {

	
	void addNoteList(final Collection<Note> noteList);

	
	void tryRemoveNoteList(final Collection<Note> noteList) throws ATMOutOfNotesException;

	
	void removeNoteList(final Collection<Note> noteList) throws ATMOutOfNotesException;

	Map<Note, Long> getDeposit();
}
