package codingtest.com.modulr.service.note;

import java.util.Collection;

import codingtest.com.modulr.exceptions.WithdrawalAmountTranslationToNotesException;
import codingtest.com.modulr.utility.Note;


/**
 * @author Nisha.Vaswani
 *
 */
public interface NoteService {

	
	Collection<Note> translateWithdrawalAmount(long amount, NoteRepository noteRepository)
			throws WithdrawalAmountTranslationToNotesException;
}
