package codingtest.com.modulr.service.account;

import java.math.BigDecimal;

import codingtest.com.modulr.exceptions.AccountBalanceNotEnoughException;
import codingtest.com.modulr.exceptions.AccountNotFoundException;


/**
 * @author Nisha.Vaswani
 *
 */
public interface AccountService {
	
	BigDecimal checkBalance( String accountNumber ) throws AccountNotFoundException;
 
	void withdrawAmount( String accountNumber,	long withdrawalAmount ) throws AccountBalanceNotEnoughException, AccountNotFoundException;
}


