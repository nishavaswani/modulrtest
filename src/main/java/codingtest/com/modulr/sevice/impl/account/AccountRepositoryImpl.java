package codingtest.com.modulr.sevice.impl.account;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codingtest.com.modulr.exceptions.AccountBalanceNotEnoughException;
import codingtest.com.modulr.exceptions.AccountNotFoundException;
import codingtest.com.modulr.service.account.AccountRepository;

	
	/**
	 * @author Nisha.Vaswani
	 *
	 */ 
	public final class AccountRepositoryImpl implements AccountRepository {

		private static final Logger LOGGER = LoggerFactory.getLogger(AccountRepositoryImpl.class);

		public static final String ACCOUNT_1_NUMBER = "01001";
		public static final String ACCOUNT_2_NUMBER = "01002";
		public static final String ACCOUNT_3_NUMBER = "01003";

		public static final BigDecimal ACCOUNT_1_BALANCE = BigDecimal.valueOf(2738.59d);
		public static final BigDecimal ACCOUNT_2_BALANCE = BigDecimal.valueOf(23.00d);
		public static final BigDecimal ACCOUNT_3_BALANCE = BigDecimal.valueOf(0);

		private final Map<String, BigDecimal> accountBalanceMap = new LinkedHashMap<>();

		public static final AccountRepository DEFAULT = create();
 
		static AccountRepositoryImpl create() {
			return new AccountRepositoryImpl();
		}

		private AccountRepositoryImpl() {
			accountBalanceMap.put(ACCOUNT_1_NUMBER, ACCOUNT_1_BALANCE);
			accountBalanceMap.put(ACCOUNT_2_NUMBER, ACCOUNT_2_BALANCE);
			accountBalanceMap.put(ACCOUNT_3_NUMBER, ACCOUNT_3_BALANCE);
		}

		
		@Override
		public BigDecimal checkBalance(final String accountNumber ) throws AccountNotFoundException {
			LOGGER.info( "Retrieving account with number '{}'", accountNumber );
			final BigDecimal accountBalance = accountBalanceMap.get(accountNumber);
			if (accountBalance == null) {
				LOGGER.info("Account with number '{}' does not exist", 	accountNumber );
				throw new AccountNotFoundException(accountNumber);
			}
			return accountBalance;
		}

	
		@Override
		public void withdrawAmount( final String accountNumber, final long withdrawalAmount ) throws AccountNotFoundException, AccountBalanceNotEnoughException {
			LOGGER.info( "Withdrawing amountParameter of '{}' from account with number '{}'", withdrawalAmount, accountNumber );
			final BigDecimal accountBalance = checkBalance(accountNumber);
			
			if ( //
			accountBalance.compareTo(BigDecimal.valueOf(withdrawalAmount)) < 0 //
			) {
				LOGGER.info( "Account with number '{}' does not have enough balance", accountNumber );
				throw new AccountBalanceNotEnoughException( accountNumber, accountBalance, 	withdrawalAmount);
			}
			final BigDecimal newAccountBalance = accountBalance.subtract(BigDecimal.valueOf(withdrawalAmount) );
			accountBalanceMap.put( 	accountNumber, newAccountBalance );
			LOGGER.info( //
					"Amount of '{}' was withdrawn from account with number '{}'", 	withdrawalAmount, accountNumber );
		}
	}


