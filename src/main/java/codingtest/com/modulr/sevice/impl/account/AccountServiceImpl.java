package codingtest.com.modulr.sevice.impl.account;

import java.math.BigDecimal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import codingtest.com.modulr.exceptions.AccountBalanceNotEnoughException;
import codingtest.com.modulr.exceptions.AccountNotFoundException;
import codingtest.com.modulr.model.impl.account.AccountRepositoryImpl;
import codingtest.com.modulr.service.account.AccountRepository;
import codingtest.com.modulr.service.account.AccountService;


/**
 * @author Nisha.Vaswani
 *
 */
public class AccountServiceImpl implements AccountService  {
	

		private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);

		
		private final AccountRepository accountRepository;

		public AccountServiceImpl() {
			accountRepository = AccountRepositoryImpl.DEFAULT;
		}

		public AccountServiceImpl(final AccountRepository accountRepository) {
			this.accountRepository = accountRepository;
		}

		
		@Override
		public BigDecimal checkBalance(	final String accountNumber) throws AccountNotFoundException {
			LOGGER.info("Retrieving account with number '{}'",accountNumber //
			);
			final BigDecimal accountBal = accountRepository.checkBalance(accountNumber);
			return accountBal;
		}

		
		@Override
		public void withdrawAmount(final String accountNumber,final long withdrawalAmount ) throws AccountBalanceNotEnoughException, //
				AccountNotFoundException {
			LOGGER.info("Withdrawing amountParameter of '{}' from account with number '{}'", withdrawalAmount, accountNumber);
			accountRepository.withdrawAmount(accountNumber, withdrawalAmount);
		}
	}



