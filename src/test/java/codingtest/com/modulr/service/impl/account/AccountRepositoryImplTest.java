package codingtest.com.modulr.service.impl.account;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codingtest.com.modulr.exceptions.AccountBalanceNotEnoughException;
import codingtest.com.modulr.exceptions.AccountNotFoundException;
import codingtest.com.modulr.model.impl.account.AccountRepositoryImpl;


/**
 * @author Nisha.Vaswani
 *
 */
public class AccountRepositoryImplTest {

	private AccountRepositoryImpl accountRepository;


	@Before
	public void setup() {
		accountRepository = AccountRepositoryImpl.create();

	}

	@Test(expected =  AccountNotFoundException.class )
	public void callCheckBalanceAccountNotFound() throws AccountNotFoundException {
		accountRepository.checkBalance("122");
	}

	@Test
	public void checkBalance1() throws AccountNotFoundException {
		final BigDecimal accountBal = accountRepository.checkBalance((AccountRepositoryImpl.ACCOUNT_1_NUMBER));
		Assert.assertEquals(accountBal, AccountRepositoryImpl.ACCOUNT_1_BALANCE);
	}

	@Test
	public void checkBalance2() throws AccountNotFoundException {
		final BigDecimal accountBalance = accountRepository.checkBalance(AccountRepositoryImpl.ACCOUNT_2_NUMBER);
		Assert.assertEquals(accountBalance, AccountRepositoryImpl.ACCOUNT_2_BALANCE);

	}

	@Test
	public void checkBalance3() throws AccountNotFoundException {
		final BigDecimal accountBalance = accountRepository.checkBalance(AccountRepositoryImpl.ACCOUNT_3_NUMBER);
		Assert.assertEquals(accountBalance, AccountRepositoryImpl.ACCOUNT_3_BALANCE);

	}

	@Test
	public void withdrawAmount() throws AccountNotFoundException, AccountBalanceNotEnoughException {
		accountRepository.withdrawAmount(AccountRepositoryImpl.ACCOUNT_1_NUMBER, 500L);
		final BigDecimal accountBal = accountRepository.checkBalance((AccountRepositoryImpl.ACCOUNT_1_NUMBER));
		Assert.assertEquals(accountBal, AccountRepositoryImpl.ACCOUNT_1_BALANCE.subtract( 
				BigDecimal.valueOf(500L) //
				) //
				);
	}

	@Test(expected= AccountBalanceNotEnoughException.class )
	public void withdrawAmountNotEnoughBalance() throws AccountNotFoundException, AccountBalanceNotEnoughException {
		accountRepository.withdrawAmount(AccountRepositoryImpl.ACCOUNT_1_NUMBER, 5000L);
	}
}
