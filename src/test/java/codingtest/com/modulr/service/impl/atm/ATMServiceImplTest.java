package codingtest.com.modulr.service.impl.atm;

import java.math.BigDecimal;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import codingtest.com.modulr.exceptions.ATMOutOfNotesException;
import codingtest.com.modulr.exceptions.AccountBalanceNotEnoughException;
import codingtest.com.modulr.exceptions.AccountNotFoundException;
import codingtest.com.modulr.exceptions.WithdrawalAmountNotAllowedException;
import codingtest.com.modulr.exceptions.WithdrawalAmountTranslationToNotesException;
import codingtest.com.modulr.service.account.AccountService;
import codingtest.com.modulr.service.impl.note.NoteServiceImpl;
import codingtest.com.modulr.service.note.NoteRepository;
import codingtest.com.modulr.service.note.NoteService;
import codingtest.com.modulr.utility.BalanceFormatter;
import codingtest.com.modulr.utility.Note;


/**
 * @author Nisha.Vaswani
 *
 */
public class ATMServiceImplTest {

	private static final String ACCOUNT_NUMBER = "01001";
	private static final long WITHDRAWAL_AMOUNT = 100L;
	private static final long WITHDRAWAL_AMOUNT_TOO_SMALL = 10L;
	private static final long WITHDRAWAL_AMOUNT_TOO_LARGE = 300L;

	private AtmServiceImpl service;

	private AccountService accountService;
	private NoteService noteService;
	private NoteRepository noteRepository;
	private BalanceFormatter balanceFormatter;

	private BigDecimal accountBal=new BigDecimal(23);
	private Collection<Note> withdrawalNoteList;

	@Before
	public void setup() throws AccountNotFoundException, WithdrawalAmountTranslationToNotesException {
		accountService = Mockito.mock(AccountService.class);
		noteService = Mockito.mock(NoteService.class);
		noteRepository = Mockito.mock(NoteRepository.class);
		balanceFormatter = Mockito.mock(BalanceFormatter.class);
	
		Mockito.doReturn( accountBal ).when( accountService ).checkBalance( ACCOUNT_NUMBER );
		withdrawalNoteList = new NoteServiceImpl().translateWithdrawalAmount( WITHDRAWAL_AMOUNT, noteRepository);
		Mockito.doReturn( withdrawalNoteList ).when( noteService ).translateWithdrawalAmount( Mockito.eq(WITHDRAWAL_AMOUNT), Mockito.same(noteRepository) );
		service = new AtmServiceImpl( accountService, noteService, noteRepository, balanceFormatter );
	}

	@Test
	public void checkBalance() throws AccountNotFoundException {
		service.checkBalance(ACCOUNT_NUMBER);

		final InOrder inOrder =	Mockito.inOrder(accountService, balanceFormatter );
		inOrder.verify( accountService ).checkBalance(	Mockito.same(ACCOUNT_NUMBER) );
		inOrder.verify( balanceFormatter ).formatBalance( Mockito.same(accountBal) //
		);
	}

	@Test
	public void withdrawAmount() throws AccountNotFoundException, AccountBalanceNotEnoughException, ATMOutOfNotesException, WithdrawalAmountTranslationToNotesException, WithdrawalAmountNotAllowedException {
		service.withdrawAmount( ACCOUNT_NUMBER, WITHDRAWAL_AMOUNT );

		final InOrder inOrder = Mockito.inOrder( noteService, 	noteRepository, accountService );
		inOrder.verify( noteService	).translateWithdrawalAmount( Mockito.eq(WITHDRAWAL_AMOUNT), Mockito.same(noteRepository) );
		inOrder.verify( noteRepository ).tryRemoveNoteList( Mockito.same(withdrawalNoteList) );
		inOrder.verify( accountService ).withdrawAmount( Mockito.same(ACCOUNT_NUMBER), Mockito.eq(WITHDRAWAL_AMOUNT) );
		inOrder.verify( noteRepository ).removeNoteList( Mockito.same(withdrawalNoteList) 
		);
	}

	@Test(expected=WithdrawalAmountNotAllowedException.class )
	public void withdrawAmountTooSmall() throws AccountNotFoundException, AccountBalanceNotEnoughException, ATMOutOfNotesException, WithdrawalAmountTranslationToNotesException, WithdrawalAmountNotAllowedException {
		service.withdrawAmount( ACCOUNT_NUMBER, WITHDRAWAL_AMOUNT_TOO_SMALL );
	}

	@Test(expected = WithdrawalAmountNotAllowedException.class )
	public void withdrawAmountTooBig() throws AccountNotFoundException, AccountBalanceNotEnoughException, ATMOutOfNotesException, WithdrawalAmountTranslationToNotesException, WithdrawalAmountNotAllowedException {
		service.withdrawAmount( ACCOUNT_NUMBER, WITHDRAWAL_AMOUNT_TOO_LARGE );
	}
}
