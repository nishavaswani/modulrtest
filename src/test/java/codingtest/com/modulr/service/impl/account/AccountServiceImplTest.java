package codingtest.com.modulr.service.impl.account;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.Test;
import org.mockito.Mockito;

import codingtest.com.modulr.exceptions.AccountBalanceNotEnoughException;
import codingtest.com.modulr.exceptions.AccountNotFoundException;
import codingtest.com.modulr.service.account.AccountRepository;
import codingtest.com.modulr.service.account.AccountService;
import codingtest.com.modulr.sevice.impl.account.AccountServiceImpl;

/**
 * @author Nisha.Vaswani
 *
 */
public class AccountServiceImplTest {


	private final AccountService accountService = new AccountServiceImpl();
	private static final String ACCOUNT_NUMBER = "number";
	private static final BigDecimal ACCOUNT_BALANCE = BigDecimal.ZERO;
	private AccountRepository accountRepository;

	@Test
	public void testCheckBalance() throws AccountNotFoundException {
		assertThat(accountService.checkBalance("01001")).isEqualTo("2738.59");
	}

	@Test(expected=AccountNotFoundException.class)
	public void checkBalance_throwsException_whenAccountNumberIsNotFound() throws AccountNotFoundException {
		// given
		String accountNumber = "1000";
		accountService.checkBalance(accountNumber);

	}

	@Test(expected=AccountNotFoundException.class)
	public void withdrawAmount() throws AccountBalanceNotEnoughException, AccountNotFoundException {
		accountService.withdrawAmount(ACCOUNT_NUMBER, ACCOUNT_BALANCE.longValue());
		Mockito.verify( accountRepository ).withdrawAmount( Mockito.same(ACCOUNT_NUMBER), 
				Mockito.eq(ACCOUNT_BALANCE.longValue()) //
				);
	}

}


