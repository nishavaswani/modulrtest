package codingtest.com.modulr.service.impl.note;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import codingtest.com.modulr.exceptions.WithdrawalAmountTranslationToNotesException;
import codingtest.com.modulr.service.impl.note.NoteRepositoryImpl;
import codingtest.com.modulr.service.impl.note.NoteServiceImpl;
import codingtest.com.modulr.service.note.NoteRepository;
import codingtest.com.modulr.utility.Note;
import codingtest.com.modulr.utility.NoteHelper;


/**
 * @author Nisha.Vaswani
 *
 */
public class NoteServiceImplTest {

	private NoteServiceImpl noteService;
	private NoteRepository noteRepository;

	@Before
	public void setup() {
		noteService = new NoteServiceImpl();
		noteRepository = new NoteRepositoryImpl();
	}

	@Test(expected =  WithdrawalAmountTranslationToNotesException.class )
	public void translateWithdrawalAmountImpossible() throws WithdrawalAmountTranslationToNotesException {
		noteService.translateWithdrawalAmount(12L, noteRepository);
	}

	@Test(expected= WithdrawalAmountTranslationToNotesException.class )
	public void translateWithdrawalAmountInsufficientNotes() throws WithdrawalAmountTranslationToNotesException {
		noteService.translateWithdrawalAmount(1000L, noteRepository);
	}


	@Test
	public void translateWithdrawalAmount_translate20asAtLeastOneFIVE() throws WithdrawalAmountTranslationToNotesException {
		final Collection<Note> noteList = NoteHelper.toNoteList(getNoteMap_20asAtLeastOneFIVE());
		noteRepository.addNoteList(noteList);
		final long amount=20L; 

		Collection<Note> noteListTranslation = null;
		try {
			noteListTranslation = noteService.translateWithdrawalAmount(amount, noteRepository);
		} catch (WithdrawalAmountTranslationToNotesException message) {
			throw message;
		}

		final Map<Note, Long> noteMapTranslation = NoteHelper.toNoteMap(noteListTranslation);
		Assert.assertEquals( noteMapTranslation, getNoteMap_20asAtLeastOneFIVE());
	}
	
	@Test
	public void translateWithdrawalAmount_translate20asTwoTen() throws WithdrawalAmountTranslationToNotesException {
		final Collection<Note> noteList = NoteHelper.toNoteList(getNoteMap_20asTwoTen());
		noteRepository.addNoteList(noteList);
		final long amount=20L; 

		Collection<Note> noteListTranslation = null;
		try {
			noteListTranslation = noteService.translateWithdrawalAmount(amount, noteRepository);
		} catch (WithdrawalAmountTranslationToNotesException message) {
			throw message;
		}

		final Map<Note, Long> noteMapTranslation = NoteHelper.toNoteMap(noteListTranslation);
		Assert.assertEquals( noteMapTranslation, getNoteMap_20asTwoTen());
	}
	
	
	
	public Map<Note, Long> getNoteMap_20asAtLeastOneFIVE() {
		final Map<Note, Long> noteMap = new LinkedHashMap<>();
		noteMap.put(Note.TEN, 1L);
		noteMap.put(Note.FIVE, 2L);
		return noteMap;
	}
	
	public Map<Note, Long> getNoteMapTranslation_20asAtLeastOneFive() {
		final Map<Note, Long> noteMapTranslation = new LinkedHashMap<>();
		noteMapTranslation.put(Note.TEN, 1L);
		noteMapTranslation.put(Note.FIVE, 2L);
		return noteMapTranslation;
	}
	
	public Map<Note, Long> getNoteMap_20asTwoTen() {
		final Map<Note, Long> noteMap = new LinkedHashMap<>();
		noteMap.put(Note.TEN, 2L);
		return noteMap;
	}
	
	public Map<Note, Long> getNoteMapTranslation_20asTwoTen() {
		final Map<Note, Long> noteMapTranslation = new LinkedHashMap<>();
		noteMapTranslation.put(Note.TEN, 2L);
		return noteMapTranslation;
	}


}
